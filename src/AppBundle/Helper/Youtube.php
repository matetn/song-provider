<?php


namespace AppBundle\Helper;


class Youtube
{
    public static function isValidYoutubeURL( $url )
    {
        $ch        = curl_init();
        $oEmbedURL = 'https://www.youtube.com/oembed?url=' . urlencode( $url ) . '&format=json';
        curl_setopt( $ch, CURLOPT_URL, $oEmbedURL );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        // Silent CURL execution
        $output = curl_exec( $ch );
        unset( $output );

        $info = curl_getinfo( $ch );
        curl_close( $ch );

        return $info[ 'http_code' ] !== 404;
    }

    public static function extractId( $url )
    {
        preg_match( "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches );

        return $matches[ 1 ];
    }
}