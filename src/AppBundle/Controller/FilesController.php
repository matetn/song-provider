<?php


namespace AppBundle\Controller;

use AppBundle\Event\UploadRequestEvent;
use AppBundle\Events;
use AppBundle\Helper\Youtube;
use AppBundle\Job\UploadFileJob;
use AppBundle\Response\Response;
use Mate\QueueBundle\Worker\Producer;
use Nette\Utils\Strings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FilesController extends Controller
{
    /**
     * @param Request         $request
     * @param EventDispatcher $eventDispatcher
     * @param Producer        $producer
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     *
     * @Route("/upload")
     */
    public function uploadAction( Request $request, EventDispatcher $eventDispatcher, Producer $producer )
    {
        // check file exists, if exists return success response
        // if not, queue upload and return success response
        $url = $request->query->get( 'url' );

        $event = new UploadRequestEvent( $request, $this->getFilesDirectory() );

        $eventDispatcher->dispatch( Events::UPLOAD_REQUEST_INITIALIZE, $event );

        if ( $response = $event->getResponse() ) {
            return $response;
        }

        // process to upload file (url, filename, filePath)
        $job = new UploadFileJob( $url, $this->getFilesDirectory() );
        $producer->produce( $job, 3 );

        $eventDispatcher->dispatch( Events::UPLOAD_REQUEST_TERMINATE, $event );

        if ( $response = $event->getResponse() ) {
            return $response;
        }

        return Response::success();
    }

    /**
     * @param Request    $request
     * @param Filesystem $filesystem
     *
     * @return BinaryFileResponse|JsonResponse
     *
     * @throws \Exception
     *
     * @Route("/download")
     */
    public function downloadAction( Request $request, Filesystem $filesystem )
    {
        $url = $request->query->get( 'url' );

        if ( !Youtube::isValidYoutubeURL( $url ) ) {
            return Response::failed( 'URL invalid', 400 );
        }


        $file = $this->getDoctrine()->getRepository( 'AppBundle:File' )->findOneBy( [
            'externalId' => Youtube::extractId( $url )
        ] );

        if ( !$file ) {
            return Response::failed( 'File not found', 404 );
        }

        if ( !$filesystem->exists( $this->getFilePath( $file->getName() ) ) ) {
            return Response::failed( 'File not found', 404 );
        }


        // set the original name for file to download
        $fileName = sprintf( '%s.mp3', $file->getOriginalName() );

        $response = new BinaryFileResponse( $this->getFilePath( $file->getName() ) );

        $invalid_characters = array('$', '%', '#', '<', '>', '|', '/');

        $fileName = str_replace($invalid_characters, '', $fileName);

        $response->setContentDisposition( ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName );

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws \Exception
     *
     * @Route("/status")
     */
    public function statusAction( Request $request )
    {
        // get filename, if exists, return its status
        // else return 404 not found

        $url = $request->query->get( 'url' );

        if ( !Youtube::isValidYoutubeURL( $url ) ) {
            return Response::failed( 'URL invalid', 400 );
        }

        $file = $this->getDoctrine()->getRepository( 'AppBundle:File' )->findOneBy( [
            'externalId' => Youtube::extractId( $url )
        ] );

        if ( !$file ) {
            return Response::failed( 'File not found', 404 );
        }

        return Response::response( $file->getStatus() );
    }


    protected function getFilePath( $filename )
    {
        $directoryPath = $this->getFilesDirectory();

        return sprintf( '%s/%s.mp3', $directoryPath, $filename );
    }

    protected function getFilesDirectory()
    {
        return $this->getParameter( 'files_directory_full_path' );
    }

    protected function getFilesPathByHost( $host, $filename )
    {
        // http://localhost/files/$filename.mp3
        return sprintf( '%s%s/%s.mp3', $host, $this->getParameter( 'files_directory' ), $filename );
    }
}