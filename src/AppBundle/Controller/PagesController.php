<?php


namespace AppBundle\Controller;


use AppBundle\Response\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PagesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/ping")
     */
    public function pingAction()
    {
        return Response::success('pong');
    }
}