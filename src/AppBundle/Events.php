<?php


namespace AppBundle;


final class Events
{
    const UPLOAD_REQUEST_INITIALIZE = 'upload.request.initialize';

    const UPLOAD_REQUEST_TERMINATE = 'upload.request.terminate';
}