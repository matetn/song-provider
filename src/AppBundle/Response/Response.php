<?php


namespace AppBundle\Response;


use AppBundle\Attribute\Status;
use Symfony\Component\HttpFoundation\JsonResponse;

class Response
{
    /**
     * @param null|string $message
     *
     * @return JsonResponse
     */
    public static function pending( $message = null )
    {
        return self::response( Status::PENDING, $message );
    }

    /**
     * @param null|string $message
     * @param int|null    $code
     *
     * @return JsonResponse
     */
    public static function failed( $message = null, $code = 200 )
    {
        return self::response( Status::FAILED, $message, $code );
    }

    /**
     * @param null|string $message
     *
     * @return JsonResponse
     */
    public static function success( $message = null )
    {
        return self::response( Status::SUCCESS, $message );
    }

    /**
     * @param             $status
     * @param null|string $message
     * @param null|int    $code
     *
     * @return JsonResponse
     */
    public static function response( $status, $message = null, $code = 200 )
    {
        return new JsonResponse( compact( 'status', 'message' ), $code );
    }
}