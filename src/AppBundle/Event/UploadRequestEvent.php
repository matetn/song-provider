<?php


namespace AppBundle\Event;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadRequestEvent extends Event
{
    /** @var Request */
    protected $request;

    /** @var Response */
    protected $response;

    /** @var string */
    protected $filesDirectory;

    /**
     * UploadRequestEvent constructor.
     *
     * @param Request $request
     * @param string  $filesDirectory
     */
    public function __construct( Request $request, $filesDirectory )
    {
        $this->request        = $request;
        $this->filesDirectory = $filesDirectory;
    }


    /**
     * @return string
     */
    public function getFilesDirectory()
    {
        return $this->filesDirectory;
    }


    /**
     * @param Response $response
     */
    public function setResponse( $response )
    {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

}