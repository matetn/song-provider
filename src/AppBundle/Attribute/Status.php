<?php


namespace AppBundle\Attribute;

class Status
{
    const SUCCESS = 1;
    const FAILED  = 2;
    const PENDING = 3;
}