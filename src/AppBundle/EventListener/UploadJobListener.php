<?php


namespace AppBundle\EventListener;


use AppBundle\Attribute\Status;
use AppBundle\Job\UploadFileJob;
use Doctrine\ORM\EntityManager;
use Mate\QueueBundle\Event\JobEvent;
use Mate\QueueBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UploadJobListener implements EventSubscriberInterface
{
    /** @var EntityManager */
    protected $entityManager;

    public function __construct( EntityManager $entityManager )
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::MATE_QUEUE_JOB_INITIALIZED => array(
                array( 'onUploadInitialized' )
            ),
            Events::MATE_QUEUE_JOB_FAILED      => array(
                array( 'onUploadFailed' )
            ),
            Events::MATE_QUEUE_JOB_EXECUTED    => array(
                array( 'onUploadFinished' )
            )
        ];
    }

    public function onUploadInitialized( JobEvent $event )
    {
        /** @var UploadFileJob $job */
        $job = $event->getJob();

        if ( !$job instanceof UploadFileJob ) {
            return;
        }

        $this->closeEntityManagerConnection();

        if ( $file = $this->getFile( $job->getExternalId() ) ) {
            $file->setName( $job->getFilename() );

            $this->getEntityManager()->persist( $file );
            $this->getEntityManager()->flush( $file );
        }

        $this->closeEntityManagerConnection();
    }

    public function onUploadFailed( JobEvent $event )
    {
        /** @var UploadFileJob $job */
        $job = $event->getJob();

        if ( !$job instanceof UploadFileJob ) {
            return;
        }

        if ( $file = $this->getFile( $job->getExternalId() ) ) {
            $file->setStatus( Status::FAILED );

            $this->getEntityManager()->persist( $file );
            $this->getEntityManager()->flush( $file );
        }
    }

    public function onUploadFinished( JobEvent $event )
    {
        /** @var UploadFileJob $job */
        $job = $event->getJob();

        if ( !$job instanceof UploadFileJob ) {
            return;
        }

        if ( $file = $this->getFile( $job->getExternalId() ) ) {
            $file
                ->setStatus( Status::SUCCESS )
                ->setOriginalName( $job->getOriginalName() );

            $this->getEntityManager()->persist( $file );
            $this->getEntityManager()->flush( $file );
        }
    }

    /**
     * @param $externalId
     *
     * @return \AppBundle\Entity\File|null|object
     */
    protected function getFile( $externalId )
    {
        return $this->getEntityManager()->getRepository( 'AppBundle:File' )->findOneBy( [
            'externalId' => $externalId
        ] );
    }

    private function getEntityManager()
    {
        if ( !$this->entityManager->isOpen() ) {
            $this->entityManager = EntityManager::create(
                $this->entityManager->getConnection(),
                $this->entityManager->getConfiguration()
            );
        }

        return $this->entityManager;
    }

    private function closeEntityManagerConnection()
    {
        $this->entityManager->close();
    }
}