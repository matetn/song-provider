<?php


namespace AppBundle\EventListener;


use AppBundle\Attribute\Status;
use AppBundle\Entity\File;
use AppBundle\Event\UploadRequestEvent;
use AppBundle\Events;
use AppBundle\Helper\Youtube;
use AppBundle\Response\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;

class UploadRequestListener implements EventSubscriberInterface
{
    /** @var Filesystem */
    protected $filesystem;

    /** @var EntityManager */
    protected $entityManager;

    public function __construct( Filesystem $filesystem, EntityManager $entityManager )
    {
        $this->filesystem    = $filesystem;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::UPLOAD_REQUEST_INITIALIZE => array(
                array( 'checkRequestParameters', 0 ),
                array( 'validateYoutubeUrl', -10 ),
                array( 'checkFileExists', -20 ),
                array( 'checkFilePending', -30 ),
                array( 'checkFileFailed', -40 )
            ),
            Events::UPLOAD_REQUEST_TERMINATE  => array(
                array( 'createFileEntity' )
            )
        ];
    }

    public function checkRequestParameters( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $request = $event->getRequest();

        if ( !$request->query->get( 'url' ) ) {
            $event->setResponse( Response::failed( 'Invalid request parameters', 400 ) );
        }
    }

    public function checkFileExists( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $url = $event->getRequest()->query->get( 'url' );

        if ( !$file = $this->getUploadedFile( $url ) ) {
            return;
        }

        $filePath = sprintf( '%s/%s.mp3', $event->getFilesDirectory(), $file->getName() );

        if ( $this->filesystem->exists( $filePath ) ) {
            $event->setResponse( Response::success( 'File already existed' ) );
        }
    }

    public function checkFilePending( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $url = $event->getRequest()->query->get( 'url' );

        if ( !$file = $this->getUploadedFile( $url ) ) {
            return;
        }

        if ( $file->getStatus() === Status::PENDING ) {
            $event->setResponse( Response::success( 'File is uploading now' ) );
        }
    }

    public function checkFileFailed( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $url = $event->getRequest()->query->get( 'url' );

        if ( !$file = $this->getUploadedFile( $url ) ) {
            return;
        }

        // Here the file was uploaded and does not exist
        // on our filesystem
        $status = $file->getStatus();

        if ( $status === Status::FAILED || $status === Status::SUCCESS ) {
            // remove file and don't set the response
            // to let retry the upload process
            $this->entityManager->remove( $file );
            $this->entityManager->flush( $file );

            return;
        }
    }

    public function validateYoutubeUrl( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $url = $event->getRequest()->query->get( 'url' );

        if ( !Youtube::isValidYoutubeURL( $url ) ) {
            $event->setResponse( Response::failed( 'URL invalid', 400 ) );
        }
    }

    public function createFileEntity( UploadRequestEvent $event )
    {
        if ( $event->getResponse() ) {
            return;
        }

        $request = $event->getRequest();
        $url     = $request->query->get( 'url' );

        $externalId = Youtube::extractId( $url );

        $file = ( new File() )
            ->setExternalId( $externalId )
            ->setStatus( Status::PENDING );

        $this->entityManager->persist( $file );
        $this->entityManager->flush( $file );
    }

    private function getUploadedFile( $url )
    {
        return $this->entityManager->getRepository( 'AppBundle:File' )->findOneBy( [
            'externalId' => Youtube::extractId( $url )
        ] );
    }
}