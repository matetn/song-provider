<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MateQueueClearCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName( 'mate:queue:clear' );;
    }

    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $worker = $this->getContainer()->get('mate.queue.worker');

        try
        {
            while($job = $worker->getConnection()->peekReady('application-queue'))
            {
                $worker->getConnection()->delete($job);
            }
        }
        catch(\Exception $e){}


        $output->writeln( '<info>All jobs was deleted successfully.</info>' );
    }

}
