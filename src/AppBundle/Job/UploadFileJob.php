<?php


namespace AppBundle\Job;


use Mate\QueueBundle\Worker\Job;
use Mate\Youtube\Entity\Video;
use Mate\Youtube\Youtube;
use AppBundle\Helper\Youtube as YoutubeHelper;

class UploadFileJob extends Job
{
    /** @var string */
    protected $url;

    /** @var string */
    protected $filename;

    /** @var string */
    protected $filesDirectory;

    /** @var string */
    protected $originalName;

    public function __construct( $url, $filesDirectory )
    {
        $this->url            = $url;
        $this->filename       = uniqid('cave_', false);
        $this->filesDirectory = $filesDirectory;
    }

    public function handle()
    {
        $youtube = new Youtube( [
            'url'      => $this->url,
            'filename' => $this->filename,
            'path'     => $this->filesDirectory
        ] );

        /** @var Video $video */
        $video = $youtube->download( function () {
            return true;
        } );

        $this->originalName = $video->getTitle();
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    public function getExternalId()
    {
        return YoutubeHelper::extractId($this->url);
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }
}